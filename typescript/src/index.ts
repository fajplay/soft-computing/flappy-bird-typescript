const acceleration: number = 0.5;
const numOfWeights: number = 4;
const populationSize: number = 100;
const width: number = window.innerWidth;
const height: number = window.innerHeight;
const diagonal: number = Math.sqrt(height * height + width * width);
const pipeGap: number = height / 3;
const pipeWidth: number = 40;
const birdsX: number = width / 10;
const birdRadius: number = pipeGap / 10;
const numOfEliteBirds: number = 1;

class NeuralNetwork {
    public weights: number[];
    public bias: number;

    constructor(initialWeights: number[], initialBias: number) {
        this.weights = initialWeights;
        this.bias = initialBias;
    }

    public sigmoid(x: number) {
        return 1 / (1 + Math.exp(-x));
    }

    public predict(inputs: number[]): number {
        const z: number =
            inputs.map((input: number, idx: number) => input * this.weights[idx]).reduce((a: number, c: number) => a + c, 0) + this.bias;
        return Math.tanh(z);
        // return this.sigmoid(z);
    }
}


const pickInRange = (min: number, max: number): number => Math.random() * (max - min) + min;
const pickIntInRange = (min: number, max: number): number => Math.round(pickInRange(min, max));

class Pipe {
    static pipes: Pipe[] = [];
    static closestPipe: Pipe | null = null;

    public bottom: number;
    public top: number;
    public x: number;
    public haveChild: boolean;

    constructor() {
        this.bottom = pickIntInRange(height * 0.025, height * 0.65);
        this.top = this.bottom + pipeGap;
        this.x = width;
        this.haveChild = false;
    }

    public move(): void {
        const speed: number = 5 + Bird.score / 100;
        this.x -= speed;
        if (!this.haveChild && this.x < width / 2) {
            this.haveChild = true;
            Pipe.pipes.push(new Pipe());
        }
        if (this.x < 0) {
            Pipe.pipes.shift();
            Bird.score++;
        }
    }

    public static calcClosestPipe(): void {
        Pipe.closestPipe = null;
        let minimumDistance: number = Infinity;
        for (const pipe of Pipe.pipes) {
            if (pipe.x + birdRadius < birdsX) continue;
            const currentDistance: number = pipe.x - birdsX;
            if (currentDistance < minimumDistance) {
                minimumDistance = currentDistance;
                Pipe.closestPipe = pipe;
            }
        }
    }
}

class Bird {
    static score: number = 0;
    static generation: number = 1;
    static birds: Bird[] = [];
    static eliteBirds: Bird[] = [];
    y: number;
    brain: NeuralNetwork;
    velocityY: number;
    isDead: boolean;

    constructor(initialWeights: number[], initialBias: number) {
        this.y = height / 2;
        this.brain = new NeuralNetwork(initialWeights, initialBias);
        this.velocityY = 0;
        this.isDead = false;
    }

    kill() {
        if (this.isDead) return;
        this.isDead = true;
        Bird.birds.splice(Bird.birds.indexOf(this), 1);
        if (Bird.birds.length < numOfEliteBirds) {
            Bird.eliteBirds.push(this);
        }
    }

    move() {
        this.velocityY -= acceleration;
        this.y = Math.min(Math.max(this.y + this.velocityY, 0), height);
        if (Pipe.closestPipe) {
            if (
                this.y < birdRadius ||
                this.y > height - birdRadius ||
                ((this.y + birdRadius > Pipe.closestPipe.top ||
                    this.y - birdRadius < Pipe.closestPipe.bottom) &&
                    Math.abs(birdsX - Pipe.closestPipe.x) < pipeWidth / 2 + birdRadius)
            ) {
                this.kill();
                return;
            }
            if (
                this.brain.predict([
                    this.y / height,
                    ...this.getDistanceToClosestPipe().map(dis => (dis / diagonal) * 2),
                    (Pipe.closestPipe.x - birdsX) / width
                ]) > 0.5
            ) {
                this.jump();
            }
        }
    }

    jump() {
        this.velocityY = pipeGap / 25;
    }

    getDistanceToClosestPipe(): number[] {
        if (!Pipe.closestPipe) {
            return [0, 0];
        }
        return [
            Math.sqrt(
                Math.pow(Pipe.closestPipe.x - birdsX, 2) + Math.pow(Pipe.closestPipe.top - this.y, 2)
            ),
            Math.sqrt(
                Math.pow(Pipe.closestPipe.x - birdsX, 2) + Math.pow(Pipe.closestPipe.bottom - this.y, 2)
            )
        ];
    }
}

const pickRandom = (arr: string | any[]) => arr[Math.floor(Math.random() * arr.length)];

const mutate = (value: number, rate: number) => value + pickInRange(-rate, rate);

function generateBirds() {
    if (Bird.eliteBirds.length) {
        const newWeights = Array.from({ length: numOfWeights }).map((_, idx) =>
            pickRandom(Bird.eliteBirds.map(bird => bird.brain.weights[idx]))
        );
        const newBias = pickRandom(Bird.eliteBirds.map(bird => bird.brain.bias));
        const rate = Math.pow(0.5, Math.log(Bird.score || 1));
        console.log("rate: ", rate);
        Bird.birds = Array.from({ length: populationSize }).map(
            () => new Bird(newWeights.map(mutate, rate), mutate(newBias, rate))
        );
    } else {
        Bird.birds = Array.from({ length: populationSize }).map(
            () =>
                new Bird(
                    Array.from({ length: numOfWeights }).map(() => pickInRange(-1, 1)),
                    Math.random()
                )
        );
    }
}

const canvas = document.querySelector<HTMLCanvasElement>("canvas");
const ctx = canvas?.getContext("2d");

if (!ctx) {
    throw new Error("Cannot get canvas context");
}

function draw(): void {
    if (!ctx) {
        throw new Error("Cannot get canvas context");
    }
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, width, height);
    ctx.fill();
    if (Bird.birds.length === 0) {
        generateBirds();
        Pipe.pipes = [];
        Bird.eliteBirds = [];
        Bird.generation++;
        Bird.score = 0;
        Pipe.pipes.push(new Pipe());
        return;
    }
    for (const pipe of Pipe.pipes) {
        pipe.move();
    }
    Pipe.calcClosestPipe();
    for (const bird of Bird.birds) {
        bird.move();
    }
    ctx.fillStyle = "white";
    for (const pipe of Pipe.pipes) {
        ctx.beginPath();
        ctx.rect(pipe.x - pipeWidth / 2, 0, pipeWidth, height - pipe.top);
        ctx.rect(pipe.x - pipeWidth / 2, height - pipe.bottom, pipeWidth, pipe.bottom);
        ctx.fill();
        ctx.stroke();
    }
    ctx.fillStyle = "#1565c060";
    ctx.strokeStyle = "#d5000090";
    for (const bird of Bird.birds) {
        ctx.beginPath();
        ctx.arc(birdsX, height - bird.y, birdRadius, 0, 2 * Math.PI);
        if (Pipe.closestPipe) {
            ctx.moveTo(birdsX, height - bird.y);
            ctx.lineTo(Pipe.closestPipe.x, height - Pipe.closestPipe.bottom);
            ctx.moveTo(birdsX, height - bird.y);
            ctx.lineTo(Pipe.closestPipe.x, height - Pipe.closestPipe.top);
            ctx.stroke();
        }
        ctx.fill();
    }
    ctx.beginPath();
    ctx.fillStyle = "#ffffff";
    ctx.fillText(Bird.score.toString(), width - 150, 80);
    ctx.fillText("G - " + Bird.generation.toString(), width - 300, height - 40);
    ctx.fill();
}



let drawInterval: number | undefined;
function setup(): void {
    canvas?.setAttribute("height", `${height}`);
    canvas?.setAttribute("width", `${width}`);
    generateBirds();
    Pipe.pipes.push(new Pipe());
    if (ctx) {
        ctx.font = "80px serif";
    }
    drawInterval = setInterval(draw, 10);
}

function stop(): void {
    clearInterval(drawInterval);
}

document.body.onload = (): number | void => setTimeout(setup, 100);





